import java.util.Vector;

public class VectorStack {
	private Vector<Object> v;
	private int counter;

	public VectorStack(){
		v = new Vector<Object>();
	}
	
	//size of the stack
	public int getSize(){
		return counter;
	}
	
	//check if it is empty
	public boolean isEmpty(){
		return counter==0;
	}
	
	//push into the stack
	public Object push(Object obj){
		v.addElement(obj);
		++counter;
		return obj;
	}
	
	//pop the stack
	public Object pop(){
		if(counter == 0){
			return null;
		}else{
			Object obj=v.elementAt(v.size()-counter);
			--counter;
			return obj;
		}
	}
	
	public void restore(){
		counter=v.size();
	}
	
//	//check the deepest of the stack
//	public Object peek(){
//		if(counter == 0){
//			System.out.println("error");
//			return null;
//		}else{
//			--counter;
//			return v.elementAt(counter);
//		}
//	}
	
	
}
