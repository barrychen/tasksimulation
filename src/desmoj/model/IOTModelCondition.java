package desmoj.model;

import desmoj.core.simulator.Model;
import desmoj.core.simulator.ModelCondition;

public class IOTModelCondition extends ModelCondition {

	private IOTSimulationModel model;


	public IOTModelCondition(Model owner, String name, boolean showInTrace,
			Object[] args) {
		super(owner, name, showInTrace, args);
		model=(IOTSimulationModel) owner;
	}

	@Override
	public boolean check() {
		return model.endSimulation();
	}


}
