package desmoj.model;


import desmoj.core.dist.ContDist;
import desmoj.core.dist.ContDistNormal;
import desmoj.core.dist.ContDistUniform;
import desmoj.core.dist.DiscreteDist;
import desmoj.core.dist.DiscreteDistPoisson;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.Queue;
import desmoj.core.simulator.TimeInstant;
import desmoj.entity.CustomerGroupEntity;
import desmoj.entity.TableEntity;
import desmoj.entity.WaitressEntity;
import desmoj.event.ArrivalEvent;

public class PastryShopSimulationModel extends Model {

	private static final int WAITRESSES = 1;
	private static final int TABLES = 3;

	private Queue<CustomerGroupEntity> buyersQueue;
	private Queue<CustomerGroupEntity> eatersQueue;
	private Queue<WaitressEntity> idleWaitressQueue;
	private Queue<TableEntity> idleTableQueue;

	// Here we only specify that it is a discrete or continuous distribution
	private DiscreteDist<?> customerGroupSize;
	private ContDist customerArrivalTime;
	private ContDist buyTime;
	private ContDist eatTime;
	
	
	public PastryShopSimulationModel(Model owner, String name,boolean showInReport, boolean showInTrace) {
        super(owner, name, showInReport, showInTrace);
    }
	
	@Override
	public String description() {
		return "Pastry shop simulator";
	}

	@Override
	public void doInitialSchedules() {
		System.out.println("doInitialSchedules");
		ArrivalEvent arrivalEvent = new ArrivalEvent(this, "Arrival event", true);
	    arrivalEvent.schedule(new TimeInstant(0));
	}

	@Override
	public void init() {
		System.out.println("init");
		// Init queues
	    buyersQueue = new Queue<CustomerGroupEntity>(this, "Buyers Queue", true, false);
	    eatersQueue = new Queue<CustomerGroupEntity>(this, "Eaters Queue", true, false);
	    idleWaitressQueue = new Queue<WaitressEntity>(this, "Idle Waitresses Queue", true, false);
	    idleTableQueue = new Queue<TableEntity>(this, "Idle Tables Queue", true, false);

	    // Init random number generators
	    customerGroupSize = new DiscreteDistPoisson(this, "Customer group size", 10, true, false);
	    customerArrivalTime = new ContDistNormal(this, "Customer arrival time", 4*60, 1*60, true, false);
	    buyTime = new ContDistUniform(this, "Buying time", 1*60, 3*60, true, false);
	    eatTime = new ContDistUniform(this, "Eating time", 5*60, 20*60, true, false);
	    for (int i = 0; i < WAITRESSES; i++){
	        idleWaitressQueue.insert(new WaitressEntity(this, "Waitress", false));
	    }

	    for (int i = 0; i < TABLES; i++){
	        idleTableQueue.insert(new TableEntity(this, "Table", false));
	    }
	}

	public int getCustomerGroupSize(){
        int value = customerGroupSize.sample().intValue();
        return value == 0 ? 1 : value;
    }

    public int getCustomerArrivalTime(){
        int value = customerArrivalTime.sample().intValue();
        return value <= 0 ? 0 : value;
    }

    public int getBuyTime(){
        return buyTime.sample().intValue();
    }

    public int getEatTime(){
        return eatTime.sample().intValue();
    }

	public Queue<CustomerGroupEntity> getBuyersQueue() {
		return buyersQueue;
	}

	public Queue<WaitressEntity> getIdleWaitressQueue() {
		return idleWaitressQueue;
	}

	public Queue<CustomerGroupEntity> getEatersQueue() {
		return eatersQueue;
	}

	public Queue<TableEntity> getIdleTableQueue() {
		return idleTableQueue;
	}

    
}
