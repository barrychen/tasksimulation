package desmoj.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import desmoj.core.dist.ContDist;
import desmoj.core.dist.ContDistUniform;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.Queue;
import desmoj.core.simulator.TimeInstant;
import desmoj.entity.TaskEntity;
import desmoj.entity.TaskEntityComparator;
import desmoj.event.TasksAllocationEvent;
import desmoj.event.TickTimeEvent;
import desmoj.util.Constants;


/**
 * 20-200 APPs
 * 1 APP - 2-5 chain
 * control which queue of node in fog or cloud should incoming task put in
 * shortest waiting time of queues in every node and smaller transmission time to fog or cloud if offLoad
 * if queue exceed threadHold, offLoad to cloud
 * shortest remaining time for scheduling tasks into different node of fog
 */

public class IOTSimulationModel extends Model {

	private HashMap<Integer,Queue<TaskEntity>> parallelTasksPool;
	private HashMap<Integer,Queue<TaskEntity>> parallelLocalExecutionTasks;
	private HashMap<Integer,Queue<TaskEntity>> parallelFogExecutionTasks;
	private HashMap<Integer,Queue<TaskEntity>> parallelCloudExecutionTasks;
	private HashMap<Integer,TaskEntity> fogCache;
	private Queue<TaskEntity> localtoFogTransmissionQueue;
	private Queue<TaskEntity> localToCloudTransmissionQueue;
	private HashMap<Integer,Queue<TaskEntity>> fogTaskTransferQueue;
	private int localSpeed=1;
	private int fogSpeed=2;
	private int cloudSpeed=10;
	private int fogNode=2;
	private int cloudNode=5;
	private AtomicInteger[] latestFogQueueTime;
	private AtomicInteger[] latestCloudQueueTime;
	private AtomicInteger[] latestFogTransferQueueTime;
	private int localToFogSpeed=200;
	private int fogToCloudSpeed=100;
	private ContDist localTasksSize;
	private ContDist fogTasksSize;
//	private ParetoDistribution fogTasksSize;
	private ContDist cloudTasksSize;
//	private ParetoDistribution cloudTasksSize;
	private int taskAllocTime=1;
	private int totalOfTasks;// we make the relation between total and portion be divisible
	private AtomicInteger aTotalOfTasks;// we make the relation between total and portion be divisible
	private double portionOfFogTask=1.0;
	private int parallelChain=1;
	private int threadHold=20;// task threadHold for each node in the fog
	private double portionOfMovingTask;
	
	
	public IOTSimulationModel(Model owner, String name,int totalOfTasks, int fogSpeed,int cloudSpeed,int taskAllocTime,boolean showInReport, boolean showInTrace) {
        super(owner, name, showInReport, showInTrace);
        this.totalOfTasks=totalOfTasks;
        this.parallelTasksPool=new HashMap<Integer, Queue<TaskEntity>>();
        this.parallelLocalExecutionTasks=new HashMap<Integer, Queue<TaskEntity>>();
        this.parallelFogExecutionTasks=new HashMap<Integer, Queue<TaskEntity>>();
        this.parallelCloudExecutionTasks=new HashMap<Integer, Queue<TaskEntity>>();
        this.fogTaskTransferQueue=new HashMap<Integer, Queue<TaskEntity>>();
        this.latestFogQueueTime=new AtomicInteger[fogNode+1];
        this.latestFogTransferQueueTime=new AtomicInteger[fogNode];
        for (int i=1;i<=fogNode;i++){
        	this.latestFogQueueTime[i]=new AtomicInteger(0);
        }
        for (int i=1;i<fogNode;i++){
        	this.latestFogTransferQueueTime[i]=new AtomicInteger(0);
        }
        this.latestCloudQueueTime=new AtomicInteger[cloudNode+1];
        for (int i=1;i<=cloudNode;i++){
        	this.latestCloudQueueTime[i]=new AtomicInteger(0);
        }
        this.fogSpeed=fogSpeed;
        this.cloudSpeed=cloudSpeed;
        this.taskAllocTime=taskAllocTime;
        this.fogCache=new HashMap<Integer, TaskEntity>((int) (totalOfTasks/0.75)+1);
    }
	
	public IOTSimulationModel(Model owner, String name,int totalOfTasks, int fogSpeed,int cloudSpeed,int taskAllocTime,double portionOfMovingTask,boolean showInReport, boolean showInTrace) {
		this(owner,name,totalOfTasks,fogSpeed,cloudSpeed,taskAllocTime,showInReport,showInTrace);
		this.portionOfMovingTask=portionOfMovingTask;
	}

	@Override
	public String description() {
		return "IOT simulator";
	}

	@Override
	public void doInitialSchedules() {
		System.out.println("IOT simulator doInitialSchedules");
		TasksAllocationEvent allocEvent = new TasksAllocationEvent(this, Constants.TASKALLOCEV, false);
		allocEvent.schedule(new TimeInstant(0));	
		TickTimeEvent tickTimeEvent=new TickTimeEvent(this, Constants.TICKCLOCKEV, false);
		tickTimeEvent.schedule(new TimeInstant(0));
//		CheckQueuelenEvent checkQueuelenEvent=new CheckQueuelenEvent(this,"CheckQueuelenEvent",false);
//		checkQueuelenEvent.schedule(new TimeInstant(0));
	}

	@Override
	public void init() {
		System.out.println("IOT simulator init");
//	    taskAllocTime=new ContDistNormal(this,Constants.TASKTOBEEXEC,4,1,true,false);
	    localTasksSize=new ContDistUniform(this,Constants.LOCALTASKS,1,5,true,false);
//	    fogTasksSize=new ParetoDistribution();
	    fogTasksSize=new ContDistUniform(this,Constants.FOGTASKS,1,50,true,false);
	    cloudTasksSize=new ContDistUniform(this,Constants.CLOUDTASKS,15,25,true,false);
//	    cloudTasksSize=new ParetoDistribution();
	    initTasks();
	}
	
	private void initTasks(){
	    int count=0;
	    Random random=new Random();
	    for(int i=1;i<=parallelChain;i++){
	    	List<TaskEntity> temp=new ArrayList<TaskEntity>(totalOfTasks);
	    	int temp1=(int) (totalOfTasks*portionOfFogTask);
    		int z=(int) (temp1*portionOfMovingTask);
    		int zr=temp1-z;
    		for (int a=0;a<z;a++){
    			TaskEntity task=new TaskEntity(this,Constants.FOG,false);
    			int start=1;
    			int end=random.nextInt(fogNode)+1;
    			if (start==end){
    				end+=1;
    			}
    			task.setStart(start);
    			task.setEnd(end);
    			temp.add(task);
    		}
    		for (int b=0;b<zr;b++){
    			TaskEntity task=new TaskEntity(this,Constants.FOG,false);
    			task.setStart(1);
    			task.setEnd(1);
    			temp.add(task);
    		}
	    	int temp2=totalOfTasks-temp1;
	    	for (int j=0;j<temp2;j++){
	    		temp.add(new TaskEntity(this,Constants.CLOUD,false));
	    	}
	    	temp.sort(new TaskEntityComparator());
	    	Queue<TaskEntity> tq=new Queue<TaskEntity>(this,Constants.TASKS+" "+i,true,false);
		    for (int j = 0; j < totalOfTasks; j++){
		    	tq.insert(temp.get(j));
		    }
		    count+=totalOfTasks;
	    	parallelTasksPool.put(i,tq);
	    	parallelLocalExecutionTasks.put(i,new Queue<TaskEntity>(this,Constants.LOCALEXECTASKS+" "+i,false,false));
	    }
	    localtoFogTransmissionQueue=new Queue<TaskEntity>(this,Constants.LOCALTOFOGQ,true,false);
	    localToCloudTransmissionQueue=new Queue<TaskEntity>(this,Constants.LOCALTOCLOUDQ,true,false);
    	for (int i=1;i<=fogNode;i++){
    		parallelFogExecutionTasks.put(i,new Queue<TaskEntity>(this,Constants.FOGEXECTASKS+" "+i,true,false));
    	}
    	for (int i=1;i<=cloudNode;i++){
    		parallelCloudExecutionTasks.put(i,new Queue<TaskEntity>(this,Constants.CLOUDEXECTASKS+" "+i,true,false));
    	}
    	for (int i=1;i<fogNode;i++){
    		fogTaskTransferQueue.put(i,new Queue<TaskEntity>(this,String.format("Fog %d transfer queue", i),true,false));
    	}
	    aTotalOfTasks=new AtomicInteger(count);
	}

	public int getFogSpeed() {
		return fogSpeed;
	}
	
	public int getCloudSpeed() {
		return cloudSpeed;
	}
	
	public int getLocalSpeed() {
		return localSpeed;
	}

	public TaskEntity getSingleTask(int chainIndex) {
		return parallelTasksPool.get(chainIndex).removeFirst();
	}

	public int getParallelChain() {
		return parallelChain;
	}

	public int getLocalTasksSize() {
		return localTasksSize.sample().intValue();
	}
	
	public int getFogTasksSize() {
		return fogTasksSize.sample().intValue();
	}

	public int getCloudTasksSize() {
		return cloudTasksSize.sample().intValue();
	}

	public double getPortionOfFogTask() {
		return portionOfFogTask;
	}

	public int getLocalToFogSpeed() {
		return localToFogSpeed;
	}

	public int getFogToCloudSpeed() {
		return fogToCloudSpeed;
	}

	public int getTaskAllocTime() {
		return taskAllocTime;
	}
	
	public void runParallelInLocal(int index,TaskEntity task){
		parallelLocalExecutionTasks.get(index).insert(task);
	}
	
	/**
	 * identify whether the task been moving out of range of original fog area
	 * if so, put the task into fog instances transmission
	 * @param task
	 * @return -1 if not in the original area else execution time
	 */
	public int runFogWithDestination(TaskEntity task){
		if (task.getStart()!=task.getEnd()){
			fogTaskTransferQueue.get(task.getStart()).insert(task);
			parallelFogExecutionTasks.get(task.getEnd()).insert(task);
			return -1;
		}
		task.setId(task.getStart());
		parallelFogExecutionTasks.get(task.getStart()).insert(task);
		return latestFogQueueTime[task.getStart()].addAndGet((task.getSize()/fogSpeed));
	}
	
	public int queueFogTransfer(TaskEntity task){
		return latestFogTransferQueueTime[task.getStart()].addAndGet(10);
	}
	
	/**
	 * shortest remaining time implementation with queue length control
	 * iterate all node when not all reach threadHold return -1 to indicate nodes are about to overload
	 * @param task
	 * @return next schedule time for this task
	 */
	public int runParallelInFog(TaskEntity task){
		int queueIndex=1;
		int queuelen=parallelFogExecutionTasks.get(queueIndex).length();
		System.out.printf("parallelFogExecutionTasks %d queue %d\n",queueIndex,queuelen);
		int remainingTime=queuelen>=threadHold?-1:latestFogQueueTime[1].get();
		if (remainingTime!=0){
			for (int i=2;i<=fogNode;i++){
				int queuelen3=parallelFogExecutionTasks.get(queueIndex).length();
				System.out.printf("parallelFogExecutionTasks %d queue %d\n",i,queuelen3);
				if (queuelen3>=threadHold){continue;}
				int temp=latestFogQueueTime[i].get();
				if (temp==0){
					remainingTime=temp;
					queueIndex=i;
					break;
				}
				if (remainingTime<=temp){continue;}
				remainingTime=temp;
				queueIndex=i;
			}
		}
		if (remainingTime==-1){return remainingTime;}
		task.setId(queueIndex);
		parallelFogExecutionTasks.get(queueIndex).insert(task);
		return latestFogQueueTime[queueIndex].addAndGet((task.getSize()/fogSpeed));
	}
	
	/**
	 * shortest remaining time implementation with no queue length control
	 * iterate all node when not all reach threadHold
	 * @param task
	 * @return next schedule time for this task
	 */
	public int runParallelInFogNoQueueLength(TaskEntity task){
		int queueIndex=1;
		int remainingTime=latestFogQueueTime[1].get();
		if (remainingTime!=0){
			for (int i=2;i<=fogNode;i++){
				int temp=latestFogQueueTime[i].get();
				if (temp==0){
					remainingTime=temp;
					queueIndex=i;
					break;
				}
				if (remainingTime<=temp){continue;}
				remainingTime=temp;
				queueIndex=i;
			}
		}
		task.setId(queueIndex);
		parallelFogExecutionTasks.get(queueIndex).insert(task);
		return latestFogQueueTime[queueIndex].addAndGet((task.getSize()/fogSpeed));
	}
	
	public int runParallelInCloud(TaskEntity task){
		int queueIndex=1;
		int remainingTime=latestCloudQueueTime[1].get();
		if (remainingTime!=0){
			for (int i=2;i<=cloudNode;i++){
				int temp=latestCloudQueueTime[i].get();
				if (temp==0){
					remainingTime=temp;
					queueIndex=i;
					break;
				}
				if (remainingTime<=temp){continue;}
				remainingTime=temp;
				queueIndex=i;
			}
		}
		task.setId(queueIndex);
		parallelCloudExecutionTasks.get(queueIndex).insert(task);
		return latestCloudQueueTime[queueIndex].addAndGet((task.getSize()/cloudSpeed));
	}
	
	public void tickTimeClockForWaitingQueue(){
		for (int i=1;i<=fogNode;i++){
			latestFogQueueTime[i].updateAndGet(x->(x>0)?x-1:0);
		}
		for (int i=1;i<=cloudNode;i++){
			latestCloudQueueTime[i].updateAndGet(x->(x>0)?x-1:0);
		}
		for (int i=1;i<fogNode;i++){
			latestFogTransferQueueTime[i].updateAndGet(x->(x>0)?x-1:0);
		}
	}
	
	public void executeParallelLocal(int index,TaskEntity task){
		parallelLocalExecutionTasks.get(index).remove(task);
		decrementTaskNumber();
	}
	
	public void executeParallelFog(int index,TaskEntity task){
		saveIntoCache(task);
		parallelFogExecutionTasks.get(index).remove(task);
		decrementTaskNumber();
	}

	public void executeParallelFogWithNoCache(int index,TaskEntity task){
		parallelFogExecutionTasks.get(index).remove(task);
		if (task.getRouted()!=-1){
			parallelFogExecutionTasks.get(task.getRouted()).remove(task);
		}
		decrementTaskNumber();
	}
	
	public void executeParallelCloud(int index,TaskEntity task){
		parallelCloudExecutionTasks.get(index).remove(task);
		decrementTaskNumber();
	}
	
	public void decrementTaskNumber(){
		aTotalOfTasks.decrementAndGet();
	}
	
	public void communicateWithCloud(TaskEntity task){
		localToCloudTransmissionQueue.insert(task);
	}
	
	public void doneCommunicateWithCloud(TaskEntity task){
		localToCloudTransmissionQueue.remove(task);
	}
	
	public void communicateWithFog(TaskEntity task){
		localtoFogTransmissionQueue.insert(task);
	}
	
	public void doneCommunicationWithFog(TaskEntity task){
		localtoFogTransmissionQueue.remove(task);
	}
	
	public void doneCommunicationbetweenFog(int queueNum,TaskEntity task){
		fogTaskTransferQueue.get(queueNum).remove(task);
	}

	public boolean endSimulation(){
		return 0==aTotalOfTasks.get();
	}
	
	public void displayQueuelen(){
		System.out.print(parallelFogExecutionTasks.get(1).length()+",");
//		System.out.println(parallelCloudExecutionTasks.get(1).length());
	}

	private void saveIntoCache(TaskEntity task){
		System.out.println(String.format("save into cache %d", task.getSize()));
		fogCache.put(task.getSize(), task);
	}
	
	public boolean getResultFromCache(TaskEntity task){
		return fogCache.containsKey(task.getSize());
	}
	

}
