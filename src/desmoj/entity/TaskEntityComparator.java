package desmoj.entity;

import java.util.Comparator;

public class TaskEntityComparator implements Comparator<TaskEntity> {

	public TaskEntityComparator() {}

	@Override
	public int compare(TaskEntity o1, TaskEntity o2) {
		if (o1==null&&o2==null){return 0;}
		if (o1==null&&o2!=null){return -1;}
		if (o1!=null&&o2==null){return 1;}
		return Integer.compare(o1.getSize(),o2.getSize());
	}


}
