package desmoj.entity;


import desmoj.core.simulator.Entity;
import desmoj.model.IOTSimulationModel;
import desmoj.util.Constants;

public class TaskEntity extends Entity {

	private int size;
	private String taskName;
	private int id=-1;
	private int start;
	private int end;
	private int routed=-1;
	
	 
    public TaskEntity(IOTSimulationModel owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
        switch(name){
        	case Constants.LOCAL: this.size = owner.getLocalTasksSize();break;
        	case Constants.FOG: this.size = owner.getFogTasksSize();break;
        	case Constants.CLOUD: this.size = owner.getCloudTasksSize();break;
        	default:break;
        }
        this.taskName=name;
    }
    
    public TaskEntity(IOTSimulationModel owner, String name, int id,boolean showInTrace) {
        this(owner,name,showInTrace);
        this.id=id;
    }

    public int getSize() {
        return size;
    }

	public String getTaskName() {
		return taskName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getRouted() {
		return routed;
	}

	public void setRouted(int routed) {
		this.routed = routed;
	}


}
