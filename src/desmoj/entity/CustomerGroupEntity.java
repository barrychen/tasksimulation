package desmoj.entity;

import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

public class CustomerGroupEntity extends Entity {

	 private final int size;

	 
    public CustomerGroupEntity(int size, Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
        this.size = size;
    }

    public int getSize() {
        return size;
    }

	   
}
