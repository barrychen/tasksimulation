package desmoj.event;

import java.util.concurrent.TimeUnit;
import co.paralleluniverse.fibers.SuspendExecution;
import desmoj.core.simulator.ExternalEvent;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeSpan;
import desmoj.entity.TaskEntity;
import desmoj.model.IOTSimulationModel;
import desmoj.util.Constants;


public class TasksAllocationEvent extends ExternalEvent {

	private IOTSimulationModel model;
	
	
	public TasksAllocationEvent(Model owner, String name, boolean showInTrace) {
		super(owner, name, showInTrace);
		traceOff();
		model = (IOTSimulationModel) owner;
	}
	
	@Override
	public void eventRoutine() throws SuspendExecution {
		int parallelChain=model.getParallelChain();
		for (int i=1;i<=parallelChain;i++){
			TaskEntity task=model.getSingleTask(i);
			if (task!=null){
				String name=task.getTaskName();
				switch(name){
				case Constants.LOCAL:
					model.runParallelInLocal(task.getId(), task);
					ComputationEvent event=new ComputationEvent(model,Constants.LOCAL,true);
					event.schedule(task,new TimeSpan(task.getSize()/model.getLocalSpeed(),TimeUnit.SECONDS));
					break;
				case Constants.FOG:
					ComputationEvent event3=new ComputationEvent(model,Constants.FOG,true,true);
					event3.schedule(task,new TimeSpan(0,TimeUnit.SECONDS));
//					if (!model.getResultFromCache(task)){
//						ComputationEvent event3=new ComputationEvent(model,Constants.FOG,true,true);
//						event3.schedule(task,new TimeSpan(0,TimeUnit.SECONDS));
//					}else{
//						model.decrementTaskNumber();
//					}
					break;
				case Constants.CLOUD:
					ComputationEvent event4=new ComputationEvent(model,Constants.CLOUD,true,true);
					event4.schedule(task,new TimeSpan(0,TimeUnit.SECONDS));
					break;
				default:System.out.println(name);
				}
			}
		}
		schedule(new TimeSpan(model.getTaskAllocTime(),TimeUnit.SECONDS));
	}


}
