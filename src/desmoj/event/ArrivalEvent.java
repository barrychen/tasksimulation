package desmoj.event;

import java.util.concurrent.TimeUnit;

import co.paralleluniverse.fibers.SuspendExecution;
import desmoj.core.simulator.ExternalEvent;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeSpan;
import desmoj.entity.CustomerGroupEntity;
import desmoj.entity.WaitressEntity;
import desmoj.model.PastryShopSimulationModel;

public class ArrivalEvent extends ExternalEvent {

	private PastryShopSimulationModel model;
	
	
	public ArrivalEvent(Model owner, String name, boolean showInTrace) {
		super(owner, name, showInTrace);
		model = (PastryShopSimulationModel) owner;
	}

	@Override
	public void eventRoutine() throws SuspendExecution {
		System.out.println("ArrivalEvent");
	    CustomerGroupEntity customerGroup = new CustomerGroupEntity(20, getModel(), "Customer group", true);
	    if (model.getIdleWaitressQueue().isEmpty()) {
	        model.getBuyersQueue().insert(customerGroup);
	    }else {
	        WaitressEntity waitress = model.getIdleWaitressQueue().removeFirst();
	        BuyEndedEvent event = new BuyEndedEvent(getModel(), "Buy ended event", true);
	        event.schedule(customerGroup, waitress, new TimeSpan(model.getBuyTime(), TimeUnit.SECONDS));//TODO buyingtime
	    }
	    schedule(new TimeSpan(model.getCustomerArrivalTime(), TimeUnit.SECONDS));
	}


}
