package desmoj.event;

import java.util.concurrent.TimeUnit;

import co.paralleluniverse.fibers.SuspendExecution;
import desmoj.core.simulator.ExternalEvent;
import desmoj.core.simulator.TimeSpan;
import desmoj.model.IOTSimulationModel;

public class TickTimeEvent extends ExternalEvent {

	private IOTSimulationModel model;
	public TickTimeEvent(IOTSimulationModel owner, String name, boolean showInTrace) {
		super(owner, name, showInTrace);
		model=owner;
	}

	@Override
	public void eventRoutine() throws SuspendExecution {
		model.tickTimeClockForWaitingQueue();
		schedule(new TimeSpan(1,TimeUnit.SECONDS));
	}

	
}
