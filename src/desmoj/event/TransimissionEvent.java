package desmoj.event;

import java.util.concurrent.TimeUnit;
import desmoj.core.simulator.Event;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeSpan;
import desmoj.entity.TaskEntity;
import desmoj.model.IOTSimulationModel;
import desmoj.util.Constants;


public class TransimissionEvent extends Event<TaskEntity> {

	private IOTSimulationModel model;
	private String evetntType;
	private int qNumber=-1;
	
	
	public TransimissionEvent(Model owner, String name, boolean showInTrace) {
		 super(owner, name, showInTrace);
		 evetntType=name;
		 model = (IOTSimulationModel) owner;
	}
	public TransimissionEvent(Model owner, String name, int queueNumber,boolean showInTrace) {
		super(owner, name, showInTrace);
		evetntType=name;
		model = (IOTSimulationModel) owner;
		qNumber=queueNumber;
	}

	@Override
	public void eventRoutine(TaskEntity who1) {
//		int taskSize=who1.getSize();
//		System.out.printf("TransimissionEvent %s task size %d\n",evetntType,taskSize);
		int elapseTime;
		switch(evetntType){
			case Constants.FOG:
				if (qNumber==-1){
					model.doneCommunicationWithFog(who1);
				}else{
					model.doneCommunicationbetweenFog(qNumber,who1);
				}
				if ((elapseTime=model.runFogWithDestination(who1))==-1){
					who1.setRouted(who1.getEnd());
					who1.setEnd(who1.getStart());
					TransimissionEvent event=new TransimissionEvent(model,Constants.FOG,who1.getStart(),false);
					event.schedule(who1,new TimeSpan(model.queueFogTransfer(who1),TimeUnit.SECONDS));
				}else{
					ComputationEvent event=new ComputationEvent(model,Constants.FOG,true);
					event.schedule(who1,new TimeSpan(elapseTime,TimeUnit.SECONDS));
				}
//				elapseTime=model.runParallelInFog(who1);
//				if (elapseTime==-1){
//					ComputationEvent event=new ComputationEvent(model,Constants.CLOUD,true,true);
//					event.schedule(who1,new TimeSpan(0,TimeUnit.SECONDS));
//				}else{
//					ComputationEvent event=new ComputationEvent(model,Constants.FOG,true);
//					event.schedule(who1,new TimeSpan(elapseTime,TimeUnit.SECONDS));
//				}
				break;
			case Constants.CLOUD:
				model.doneCommunicateWithCloud(who1);
				elapseTime=model.runParallelInCloud(who1);
				ComputationEvent event3=new ComputationEvent(model,Constants.CLOUD,true);
				event3.schedule(who1,new TimeSpan(elapseTime,TimeUnit.SECONDS));
				break;
			default:break;
		}
	}

	
}
