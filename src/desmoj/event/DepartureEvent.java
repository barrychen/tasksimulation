package desmoj.event;

import desmoj.core.simulator.EventOf2Entities;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.Queue;
import desmoj.core.simulator.TimeSpan;
import desmoj.entity.CustomerGroupEntity;
import desmoj.entity.TableEntity;
import desmoj.model.PastryShopSimulationModel;

public class DepartureEvent extends EventOf2Entities<CustomerGroupEntity,TableEntity> {

	private PastryShopSimulationModel model;
	
	
	public DepartureEvent(Model owner, String name, boolean showInTrace) {
		 super(owner, name, showInTrace);
		 model = (PastryShopSimulationModel) owner;
	}

	@Override
	public void eventRoutine(CustomerGroupEntity who1, TableEntity who2) {
		System.out.println("DepartureEvent");
		Queue<CustomerGroupEntity> eatersQ=model.getEatersQueue();
		if (eatersQ.isEmpty()) {
			Queue<TableEntity> tableQ=model.getIdleTableQueue();
			tableQ.insert(who2);
	    }else {
	        CustomerGroupEntity customerGroup = eatersQ.removeFirst();
	        DepartureEvent event = new DepartureEvent(model, "Departure event", true);
	        event.schedule(customerGroup, who2, new TimeSpan(model.getEatTime()));
	    }
	}
	

}
