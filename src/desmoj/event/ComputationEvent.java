package desmoj.event;

import java.util.concurrent.TimeUnit;
import desmoj.core.simulator.Event;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeSpan;
import desmoj.entity.TaskEntity;
import desmoj.model.IOTSimulationModel;
import desmoj.util.Constants;


public class ComputationEvent extends Event<TaskEntity> {

	private IOTSimulationModel model;
	private String evetntType;
	private boolean mOffload;
	
	
	public ComputationEvent(Model owner, String name, boolean showInTrace) {
		 super(owner, name, showInTrace);
		 evetntType=name;
		 model = (IOTSimulationModel) owner;
	}
	
	public ComputationEvent(Model owner, String name, boolean offload, boolean showInTrace) {
		 super(owner, name, showInTrace);
		 evetntType=name;
		 mOffload=offload;
		 model = (IOTSimulationModel) owner;
	}

	@Override
	public void eventRoutine(TaskEntity who1) {
//		System.out.printf("ComputationEvent %s offload %s \n",evetntType,mOffload);
		if (mOffload){
			switch(evetntType){
				case Constants.FOG:
					model.communicateWithFog(who1);
					TransimissionEvent event=new TransimissionEvent(model,Constants.FOG,false);
//					event.schedule(who1,new TimeSpan(who1.getSize()/model.getLocalToFogSpeed(),TimeUnit.SECONDS));
					event.schedule(who1,new TimeSpan(1,TimeUnit.SECONDS));
					break;
				case Constants.CLOUD:
					model.communicateWithCloud(who1);
					TransimissionEvent event2=new TransimissionEvent(model,Constants.CLOUD,false);
//					event2.schedule(who1,new TimeSpan(who1.getSize()/model.getFogToCloudSpeed(),TimeUnit.SECONDS));
					event2.schedule(who1,new TimeSpan(1,TimeUnit.SECONDS));
					break;
				default:break;
			}
		}else{
			switch(evetntType){
				case Constants.LOCAL:
					model.executeParallelLocal(who1.getId(), who1);
					break;
				case Constants.FOG:
					model.executeParallelFogWithNoCache(who1.getId(), who1);
					break;
				case Constants.CLOUD:
					model.executeParallelCloud(who1.getId(), who1);
					break;
				default:break;
			}
		}
	}


}
