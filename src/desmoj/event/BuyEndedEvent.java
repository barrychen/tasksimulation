package desmoj.event;

import desmoj.core.simulator.EventOf2Entities;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.Queue;
import desmoj.core.simulator.TimeSpan;
import desmoj.entity.CustomerGroupEntity;
import desmoj.entity.TableEntity;
import desmoj.entity.WaitressEntity;
import desmoj.model.PastryShopSimulationModel;

public class BuyEndedEvent extends EventOf2Entities<CustomerGroupEntity,WaitressEntity> {

	private PastryShopSimulationModel model;
	
	
	public BuyEndedEvent(Model owner, String name, boolean showInTrace) {
		 super(owner, name, showInTrace);
		 model = (PastryShopSimulationModel) owner;
	}

	@Override
	public void eventRoutine(CustomerGroupEntity who1, WaitressEntity who2) {
		System.out.println("BuyEndedEvent");
		Queue<CustomerGroupEntity> buyersQ=model.getBuyersQueue();
		if (buyersQ.isEmpty()) {
			model.getIdleWaitressQueue().insert(who2);
	    }else {
	        CustomerGroupEntity customerGroup=buyersQ.removeFirst();
	        BuyEndedEvent event = new BuyEndedEvent(model, "Buy ended event", true);
	        event.schedule(customerGroup, who2, new TimeSpan(model.getBuyTime()));
	    }
		Queue<TableEntity> tableQ=model.getIdleTableQueue();
	    if(tableQ.isEmpty()) {
	    	Queue<CustomerGroupEntity> eatersQ=model.getEatersQueue();
	    	eatersQ.insert(who1);
	    }else {
	        TableEntity table=tableQ.removeFirst();
	        DepartureEvent event = new DepartureEvent(model, "Departure event", true);
	        event.schedule(who1, table, new TimeSpan(model.getEatTime()));
	    }
	}
	

}
