package desmoj;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import desmoj.core.simulator.Experiment;
import desmoj.core.simulator.TimeInstant;
import desmoj.model.IOTModelCondition;
import desmoj.model.IOTSimulationModel;
import desmoj.util.Constants;

public class IOTSimulationRunner {

	public static void main(String[] args) {
		String userDir=System.getProperty("user.dir");
		int numberOfTasks=4000;
		int taskAllocTime=1;
		int fogSpeed=2;
		int cloudSpeed=10;
		double portionOfMovingTask=0.1;
		while(portionOfMovingTask<=1){
			portionOfMovingTask=((double)Math.round(portionOfMovingTask*10))/10;
			String currentTimeStr=new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(Calendar.getInstance().getTime());
			IOTSimulationModel model = new IOTSimulationModel(null,"IOTSimulationModel",numberOfTasks,fogSpeed,cloudSpeed,taskAllocTime,portionOfMovingTask,true,false);
			Experiment experiment = new Experiment(String.format("IOTExperimentparallel_%d_%d_%d_%d_%s_%s",numberOfTasks,fogSpeed,cloudSpeed,taskAllocTime,new Double(portionOfMovingTask).toString().replace(".", ""),currentTimeStr), String.format("%s/temp", userDir));
			model.connectToExperiment(experiment);
//			experiment.setExecutionSpeedRate(1);
			experiment.traceOn(new TimeInstant(0));
			experiment.stop(new IOTModelCondition(model,Constants.IOTMODELCONDITION,false,new Object[0]));
			experiment.start();
			experiment.report();
			experiment.finish();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			portionOfMovingTask+=0.1;
		}
	}

	
}
