package desmoj.util;

public class Constants {
	
	public static final String IOTMODELCONDITION="IOT Model Condition";

	public static final String LOCAL="Local";
	public static final String FOG="Fog";
	public static final String CLOUD="Cloud";
	
	public static final String TASKALLOCEV="Task alloc event";
	public static final String COMPUEV="Computation task";
	public static final String COMMUEV="Communication task";
	public static final String TICKCLOCKEV="Time clock event";
	
	public static final String TASKS="Sensoring tasks";
	public static final String LOCALTASKS="Local tasks size";
	public static final String FOGTASKS="Fog tasks size";
	public static final String CLOUDTASKS="Cloud tasks size";

	public static final String LOCALEXECTASKS="Local mobile device";
	public static final String FOGEXECTASKS="Fog instance";
	public static final String CLOUDEXECTASKS="Cloud instance";
	public static final String LOCALTOCLOUDQ="Local to Cloud communications";
	public static final String LOCALTOFOGQ="Local to Fog communications";
	
	public static final String TASKTOBEEXEC="Time of tasks to be executed";
	public static final String TASKSIZE="Task size";


}
