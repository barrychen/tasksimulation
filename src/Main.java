import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
	private static final int localSpeed=5;
	private static final int fogSpeed=10;
	private static final int cloudSpeed=20;
	
	
	public static void main(String[] args) {
		/*total there are 6 tasks, including 2 communication tasks.
		 * the machine computation speed is ?????
		 */
		BufferedReader buff = null;
		ArrayList<String> taskType=new ArrayList<String>();
		try {
			String oneLine;
			buff=new BufferedReader(new FileReader(System.getProperty("user.dir")+"\\src\\tasks.txt"));
			while((oneLine=buff.readLine())!=null){
				oneLine=oneLine.trim();
				taskType.add(oneLine);
			}
			int i=0;
			VectorStack v = new VectorStack();
			for (String t:taskType){
				switch (t){
					case "cp":v.push(new Task(i,"cp"));i++;break;
					case "com":v.push(new CommTask(i,"com"));i++;break;
					default:break;
				}
			}
			Task cp=new Task(i,"cp");
			cp.setCompuLocation("cloud");
			v.push(cp);
			System.out.printf("The total time for only the computation task: %s\n",calculate(v));
			v.restore();
			for (int z=0;z<i;z++){
				Task task=(Task) v.pop();
				if ("com".equals(task.getTaskType()) || !"local".equals(task.getCompuLocation())){
					continue;
				}
				task.setCompuLocation("fog");
				v.restore();
				System.out.printf("The total time for only the computation task: %s\n",calculate(v));
				v.restore();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static double calculate(VectorStack v){
		double totalTime=0;
		Task task;
		while((task = (Task) v.pop())!=null){
			int amount = task.getSizeofTask();
			if(task.getTaskType().equals("cp")){
				System.out.println("computation task amount: "+amount);
				totalTime = totalTime+cpDelay(amount,task.getCompuLocation());
			}else if(task.getTaskType().equals("com")){
				System.out.println("commnication task amount: "+amount);
				totalTime = totalTime+cmDelay(amount);
			}
			System.out.println(totalTime);
		}
		return totalTime;
	}
	
	// all computation tasks is running in the handset device
	private static double cpDelay(int size,String compuLocation){
		switch (compuLocation){
			case "local": return size*1.0/localSpeed;
			case "fog": return size*1.0/fogSpeed;
			case "cloud": return size*1.0/cloudSpeed;
			default: return 0;
		}
	}
	
	private static double cmDelay(int size){
		int bandwidth = (int)((Math.random()+1)*10);
		return size*1.0/bandwidth;
	}
	

}
