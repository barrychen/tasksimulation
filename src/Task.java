
public class Task {

	private int Id;
	protected int sizeOfTask;
	protected double random;
	private String taskType;
	private String compuLocation="local";
	
	
	public Task(int id, String type){
		this.Id = id;
		// produce random task size
		random = Math.random();
		this.sizeOfTask = (int)((random+1)*30);
		//System.out.println(this.sizeOfTask);
		this.taskType = type;
	}
	
	public void setId(int s){
		this.Id = s;
	}
	
	public void setSizeOfTask(int t){
		this.sizeOfTask = t;
	}
	
	public void setTaskType(String t){
		this.taskType = t;
	}
	
	public int getId(){
		return this.Id;
	}
	
	public int getSizeofTask(){
		return this.sizeOfTask;
	}
	
	public String getTaskType(){
		return this.taskType;
	}

	public String getCompuLocation() {
		return compuLocation;
	}

	public void setCompuLocation(String compuLocation) {
		this.compuLocation = compuLocation;
	}
	
	
}
