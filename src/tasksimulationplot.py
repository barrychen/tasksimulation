import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime,os,re,math,time


ceilint=lambda n: ((n/10)+2)*10 if n%10>=5 else ((n/10)+1)*10
# acol=range(0,400,20)
# bcol=[0.9,0.9434,0.97087,0.98039,0.99015,1.0,1.0,1.0,1.0]
# acol=range(10,110,10)
# bcol=[37.8,62.9,87.9,113.2,138.05,162.35,187.3,212.35,237.5,261.95]
# # xcol=[0.9, 0.95238, 0.9901, 0.99174, 0.99291, 0.99448, 0.99548, 0.99585, 0.99617, 0.99707, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
# # ycol=[0, 15.0, 71.0, 86.0, 102.0, 133.0, 164.0, 181.0, 197.0, 255.0, 118.0, 148.0, 211.0, 226.0, 241.0, 270.0, 286.0, 302.0, 30.0, 45.0, 57.0]
# # xxcol=[0.9, 0.86957, 0.95349, 0.97087, 0.98374, 0.98413, 0.98655, 0.98765, 0.99015, 0.99125, 0.9924, 0.99293, 0.99387, 0.99449, 0.9967, 0.99739, 0.99752, 1.0, 1.0, 1.0, 1.0]
# # yycol=[0, 6.0, 12.0, 29.0, 35.0, 18.0, 64.0, 70.0, 58.0, 99.0, 76.0, 82.0, 47.0, 105.0, 88.0, 111.0, 117.0, 41.0, 53.0, 94.0, 24.0]
dirPath='D:/workspace/TaskSimulation/temp'

def extractData(searchKey,odirPath):
	reportTempFiles=os.listdir(odirPath)
	sklen=len(searchKey)
	tlist=[]
	xcol=[0.0]
	ycol=[0.0]
	for oneFile in reportTempFiles:
		if "report" in oneFile:
			with open(odirPath+'/'+oneFile,'rb') as tempRead:
				for oneLine in tempRead:
					matchedLine=re.search(searchKey,oneLine)
					if matchedLine:
						splitIndex=matchedLine.start()+sklen
						splitted=oneFile.split('_')
						arrivalRate=float(1)/float(splitted[4])
						cspeed=float(splitted[2]) if "Fog" in searchKey else float(splitted[3])
						crate=cspeed/float(((1+50)/2))
						temp=oneLine[splitIndex:].split("</td>")
						tlist.append((arrivalRate/crate,float(splitted[4]),float(temp[6][4:])))
	tlist=sorted(tlist,key=lambda (x,z,y):x)
	for (x,z,y) in tlist:
		xcol.append(x)
		ycol.append(y)
	# print tlist
	print xcol
	print ycol
	return xcol,ycol

def extractDataTotalTime():
	odirPath='D:/workspace/TaskSimulation/databackup/temp4000_2016_05_19_1-10_fogarrivaltime1fogspeed2ContDistUniformtasksizeMovingTasks'
	reportTempFiles=os.listdir(odirPath)
	tlist=[]
	xcol=[0]
	ycol=[0.0]
	for oneFile in reportTempFiles:
		if "report" in oneFile:
			with open(odirPath+'/'+oneFile,'rb') as tempRead:
				for oneLine in tempRead:
					matchedLine=re.search("(until)",oneLine)
					if matchedLine:
						splitted=oneFile.split('_')
						portionOfMvoing=int(splitted[5])*10
						totaloftime=float(oneLine[matchedLine.start()+len("until")+1:].split('<')[0][:-1])
						tlist.append((portionOfMvoing,totaloftime))
	tlist=sorted(tlist,key=lambda (x,y):x)
	for (x,y) in tlist:
		xcol.append(x)
		ycol.append(y)
	# print tlist
	print xcol
	print ycol
	return xcol,ycol

def extractFogQueueLength():
	odirPath="D:/workspace/TaskSimulation/databackup/temp4000_2016_05_19_1-10_fogarrivaltime1fogspeed2ContDistUniformtasksizeMovingTasks"
	reportTempFiles=os.listdir(odirPath)
	tlist=[]
	sklen=len("Fog instance 2")
	sklen3=len("Fog 1 transfer queue")
	for oneFile in reportTempFiles:
		if "report" in oneFile:
			with open(odirPath+'/'+oneFile,'rb') as tempRead:
				instanceList=[]
				splitted=oneFile.split('_')
				portionOfMvoing=int(splitted[5])*10
				for oneLine in tempRead:
					matchedLine=re.search("Fog instance 2",oneLine)
					matchedLine3=re.search("Fog 1 transfer queue",oneLine)
					if matchedLine:
						splitIndex=matchedLine.start()+sklen
						temp=oneLine[splitIndex+1:].split("</td>")
						instanceList.append((1,float(temp[6][4:])))
					elif matchedLine3:
						splitIndex=matchedLine3.start()+sklen3
						temp=oneLine[splitIndex+1:].split("</td>")
						instanceList.append((2,float(temp[6][4:])))
			tlist.append((portionOfMvoing,instanceList))
	tlist=sorted(tlist,key=lambda (x,y):x)
	arrivalAxis=[]
	instanceGroups={"1":[],"2":[]}
	for x,y in tlist:
		for tyn,typ in y:
			instanceGroups[str(tyn)].append(typ)
		arrivalAxis.append(x)
	print arrivalAxis
	print instanceGroups
	groups=10
	separations={}
	for i in range(1,3):
		separations[str(i)]=instanceGroups[str(i)][:groups]
	print separations
	return arrivalAxis,separations,groups
	

def draw(xcol,ycol,yycol):
	pddf=pd.DataFrame({'Const normal distribution': ycol,'ParetoDistribution':yycol},index=xcol)
	fig=plt.figure(1)
	ax=fig.add_subplot(111)
	ax.set_xlabel("arrival time")
	ax.set_ylabel("completion time")
	pddf.plot(ax=ax,title="task simulation for arrival time and completion time with const normal distribution/paretoDistribution for size of task", style="-s", legend=True, grid=True)
	# plt.xticks(np.arange(0,90000,5000))
	# plt.yticks(np.arange(0,21,1))
	plt.show()

def drawbBarchart(xaxis,yaxis,groups):
	fig,ax=plt.subplots()
	index=np.arange(groups)
	bar_width=0.2
	opacity=0.4
	rects=plt.bar(index+bar_width*(0),yaxis[str(1)],bar_width,alpha=opacity,color='blue',label='Fog instance 1')
	rects1=plt.bar(index+bar_width*(1),yaxis[str(2)],bar_width,alpha=opacity,color='yellow',label='Fog instance 2')
	rects1=plt.bar(index+bar_width*(2),yaxis[str(3)],bar_width,alpha=opacity,color='red',label='Cloud instance 1')
	rects1=plt.bar(index+bar_width*(3),yaxis[str(4)],bar_width,alpha=opacity,color='black',label='Cloud instance 2')
	plt.xlabel('Arrival time')
	plt.ylabel('Utilization rate')
	plt.title('Task simulation of Utilization rate with arrival time with ParetoDistribution for size of task')
	plt.xticks(index+bar_width,tuple([str(i+1) for i in range(groups)]))
	plt.legend()
	plt.tight_layout()
	plt.show()

	
if __name__ == '__main__':
	# xaxis,yaxis,groups=extractFogQueueLength()
	# drawbBarchart(xaxis,yaxis,groups)
	# xcol,ycol=extractDataTotalTime()
	# xcol=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
	# ycol=[0.0, 4461.0, 8458.0, 12465.0, 16466.0, 20462.0, 24463.0, 28455.0, 32457.0, 36456.0, 40459.0, 44276.0, 48013.0, 52012.0, 56011.0, 60010.0, 64009.0, 68008.0, 72007.0, 76006.0, 80005.0]
	# yycol=[0.0, 9861.0, 9130.0, 26905.0, 24670.0, 22399.0, 25339.0, 39383.0, 35612.0, 39357.0, 43375.0, 57416.0, 52388.0, 59014.0, 63644.0, 66232.0, 74652.0, 71829.0, 74963.0, 83716.0, 85668.0]
	# draw(xcol,ycol,yycol)
	# xxcol,yycol=extractData('<td>Fog instance 1</td>','D:/workspace/TaskSimulation/temp400_2016_05_06_1-20_075cloudfogservicerate')
	# time.sleep(2)
	# xxcol,yycol=extractData('<td>Cloud instance 1</td>','D:/workspace/TaskSimulation/temp400_2016_05_06_1-20_075cloudfogservicerate')
	# yupperBound=math.ceil(max(ycol))+10
	# xupperBound=math.ceil(max(xcol))+10
	# xlowerBound=math.floor(min(xcol))

	# xcol=[0.0, 0.2, 0.21052631578947367, 0.2222222222222222, 0.23529411764705882, 0.25, 0.26666666666666666, 0.2857142857142857, 0.3076923076923077, 0.3333333333333333,0.36363636363636365, 0.4, 0.4444444444444444, 0.5, 0.5714285714285714, 0.6666666666666666, 0.8, 1.0, 1.3333333333333333, 2.0, 4.0]
	# ycol=[0.0, 0.17583, 0.18508, 0.19535, 0.20684, 0.21975, 0.23439, 0.25112, 0.27042, 0.29293, 0.31953, 0.35144, 0.39043, 0.43916, 0.50179, 0.58524, 0.702, 0.87695, 15.73665, 72.53381, 129.33096]
	# y025fcol=[0.0, 0.04422, 0.04655, 0.04913, 0.05202, 0.05527, 0.05895, 0.06316, 0.06802, 0.07368, 0.08037, 0.0884, 0.09822, 0.11049, 0.12625, 0.14727, 0.17668, 0.22076, 0.53167, 1.40574, 3.53479]
	# y075ccol=[0.0, 0.05624, 0.0592, 0.06249, 0.06617, 0.0703, 0.07498, 0.08034, 0.08651, 0.09372, 0.10223, 0.11245, 0.12493, 0.14053, 0.16059, 0.18732, 0.22472, 0.2808, 0.37417, 0.56055, 15.64414]
	# y050fcol=[0.0, 0.08856, 0.09322, 0.0984, 0.10419, 0.11069, 0.11807, 0.1265, 0.13622, 0.14757, 0.16097, 0.17706, 0.19672, 0.22128, 0.25286, 0.29495, 0.35385, 0.44215, 1.52333, 4.9963, 39.02401]
	# y050ccol=[0.0, 0.03821, 0.04022, 0.04245, 0.04495, 0.04775, 0.05094, 0.05457, 0.05877, 0.06366, 0.06944, 0.07638, 0.08486, 0.09546, 0.10908, 0.12724, 0.15265, 0.19074, 0.25417, 0.37608, 2.18503]
	# y075fcol=[0.0, 0.13165, 0.13858, 0.14628, 0.15488, 0.16455, 0.17552, 0.18805, 0.2025, 0.21937, 0.2393, 0.26321, 0.29243, 0.32895, 0.37589, 0.43846, 0.52603, 0.65729, 2.90307, 29.54563, 84.60456]
	# y025ccol=[0.0, 0.01979, 0.02083, 0.02199, 0.02328, 0.02474, 0.02639, 0.02827, 0.03044, 0.03298, 0.03597, 0.03957, 0.04396, 0.04945, 0.05651, 0.06592, 0.07908, 0.09881, 0.1309, 0.15019, 0.48384]
	# yycol=[0.0, 0.07478, 0.07872, 0.08309, 0.08798, 0.09347, 0.0997, 0.10682, 0.11503, 0.12461, 0.13593, 0.14951, 0.16611, 0.18685, 0.21352, 0.24906, 0.2988, 0.37336, 0.4975, 0.74532, 33.28261]
	# pddf=pd.DataFrame({'100% task in Cloud': yycol,
	# '25% task in Fog': y025fcol,'75% task in Cloud': y075ccol,
	# "50% task in Fog":y050fcol,"50% task in Cloud":y050ccol,
	# "75% task in Fog":y075fcol,"25% task in Cloud":y025ccol,
	# '100% task in Fog':ycol},index=xcol)
	# fig=plt.figure(1)
	# ax=fig.add_subplot(111)
	# ax.set_xlabel("arrivalRate/serviceRate")
	# ax.set_ylabel("queue length of task")
	# pddf.plot(ax=ax,title="task simulation for arrival Rate and queue length of task with const normal distribution for size of task", style="-s", legend=True, grid=True, linewidth=2)
	# plt.show()

	# xcol=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
	# y100fcol=[0.0, 1405.0, 1405.0, 1405.0, 1601.0, 2000.0, 2399.0, 2798.0, 3197.0, 3596.0, 3995.0, 4394.0, 4793.0, 5192.0, 5591.0, 5990.0, 6389.0, 6788.0, 7187.0, 7586.0, 7985.0]
	# y100ccol=[0.0, 598.0, 801.0, 1200.0, 1599.0, 1998.0, 2397.0, 2796.0, 3195.0, 3594.0, 3993.0, 4392.0, 4791.0, 5190.0, 5589.0, 5988.0, 6387.0, 6786.0, 7185.0, 7584.0, 7983.0]
	# y025fcol=[0.0, 503.0, 801.0, 1200.0, 1599.0, 1998.0, 2397.0, 2796.0, 3195.0, 3594.0, 3993.0, 4392.0, 4791.0, 5190.0, 5589.0, 5988.0, 6387.0, 6786.0, 7185.0, 7584.0, 7983.0]
	# y050fcol=[0.0, 708.0, 811.0, 1200.0, 1599.0, 1998.0, 2397.0, 2796.0, 3195.0, 3594.0, 3993.0, 4392.0, 4791.0, 5190.0, 5589.0, 5988.0, 6387.0, 6786.0, 7185.0, 7584.0, 7983.0]
	# y075fcol=[0.0, 1052.0, 1052.0, 1207.0, 1599.0, 1998.0, 2397.0, 2796.0, 3195.0, 3594.0, 3993.0, 4392.0, 4791.0, 5190.0, 5589.0, 5988.0, 6387.0, 6786.0, 7185.0, 7584.0, 7983.0]
	# pddf=pd.DataFrame({'100% task in Cloud': y100ccol,"25% task in Fog":y025fcol,
	# "50% task in Fog":y050fcol,"75% task in Fog":y075fcol,"100% task in Fog":y100fcol},index=xcol)
	# fig=plt.figure(1)
	# ax=fig.add_subplot(111)
	# ax.set_xlabel("arrival time")
	# ax.set_ylabel("completion time")
	# pddf.plot(ax=ax,title="task simulation for arrival time and completion time with const normal distribution for size of task", style="-o", legend=True, grid=True, linewidth=3)
	# plt.show()

	# xcol=[0.1905, 0.29525, 0.378, 0.44925, 0.50925, 0.5625, 0.609, 0.64875, 0.68275, 0.7105, 0.7345, 0.7555, 0.77375, 0.795, 0.8165, 0.8375, 0.85625, 0.878, 0.89625, 0.9185]
	# xxcol=[0.12725, 0.191, 0.23475, 0.262, 0.2805, 0.291, 0.295, 0.29425, 0.289, 0.28125, 0.2655, 0.2445, 0.22625, 0.205, 0.1835, 0.1625, 0.14375, 0.122, 0.10375, 0.0815]
	# xxxcol=[0.8385, 0.898, 0.92675, 0.946, 0.951, 0.95725, 0.97125, 0.969, 0.9745, 0.97825, 0.981, 0.97825, 0.98025, 0.985, 0.98575, 0.98775, 0.98525, 0.98775, 0.9875, 0.98925]
	# xxxxcol=[0.08075, 0.067, 0.053, 0.04275, 0.03825, 0.03375, 0.02575, 0.02825, 0.02425, 0.02175, 0.019, 0.0215, 0.01975, 0.015, 0.01425, 0.01225, 0.01475, 0.01225, 0.0125, 0.01075]
	# lenx=len(xcol)
	# lenxxx=len(xxxcol)
	# xfcol=[(xcol[i]+xxcol[i]) for i in range(lenx)]
	# xxxfcol=[(xxxcol[i]+xxxxcol[i]) for i in range(lenxxx)]
	# print xfcol
	# print xxxfcol
	# yaxis={'1': xfcol, '2': xxxfcol}
	# groups=20
	# fig,ax=plt.subplots()
	# index=np.arange(groups)
	# bar_width=0.4
	# opacity=0.4
	# rects=plt.bar(index+bar_width*(0),yaxis[str(1)],bar_width,alpha=opacity,color='blue',label='Fog instance ContDistUniform task size')
	# rects1=plt.bar(index+bar_width*(1),yaxis[str(2)],bar_width,alpha=opacity,color='black',label='Fog instance ParetoDistribution task size')
	# plt.xlabel('Arrival time')
	# plt.ylabel('Utilization rate')
	# plt.title('Task simulation of arrival time with Overloading rate with ContDistUniform/ParetoDistribution for size of task')
	# plt.xticks(index+bar_width,tuple([str(i+1) for i in range(groups)]))
	# plt.legend()
	# plt.tight_layout()
	# plt.show()

	# fcol=[0.0, 0.625, 0.6578947368421052, 0.6944444444444444, 0.7352941176470588, 0.78125, 0.8333333333333333, 0.8928571428571428, 0.9615384615384616, 1.0416666666666665, 1.1363636363636365, 1.25, 1.3888888888888888, 1.5625, 1.7857142857142856, 2.083333333333333, 2.5, 3.125, 4.166666666666666, 6.25, 12.5]
	# ccol=[0.0, 0.125, 0.13157894736842105, 0.13888888888888887, 0.14705882352941174, 0.15625, 0.16666666666666666, 0.17857142857142855, 0.19230769230769232, 0.20833333333333331, 0.22727272727272727, 0.25, 0.27777777777777773, 0.3125, 0.3571428571428571, 0.41666666666666663, 0.5, 0.625, 0.8333333333333333, 1.25, 2.5]
	# plt.figure(1)
	# plt.subplot(211)
	# plt.plot(fcol, [0.0, 0.4074, 0.35291, 0.18265, 0.54976, 0.26886, 0.4895, 0.49661, 0.47377, 0.2213, 0.40477, 0.92841, 0.87802, 1.34369, 0.96092, 0.6497, 0.81387, 2.42002, 2.68594, 1.00982, 10.60943], 'k',label='100% task in Fog',linewidth=3)
	# plt.plot(fcol, [0.0, 0.05398, 0.02637, 0.02679, 0.05834, 0.03549, 0.02029, 0.03372, 0.05273, 0.13113, 0.03702, 0.05828, 0.06892, 0.05021, 0.75585, 0.19547, 0.28426, 0.1666, 0.31866, 0.18567, 1.27517], 'b',label='25% task in Fog',linewidth=3)
	# plt.plot(fcol, [0.0, 0.0559, 0.36015, 0.06364, 0.23202, 0.05116, 0.06251, 0.13804, 0.08733, 0.07311, 0.28253, 0.20666, 0.15473, 0.13593, 0.1862, 0.77489, 0.23584, 0.81151, 0.53162, 2.51051, 1.13971], 'g',label='50% task in Fog',linewidth=3)
	# plt.plot(fcol, [0.0, 0.22574, 0.1018, 0.08195, 0.12375, 0.24474, 0.15395, 0.61196, 0.20343, 0.17655, 0.22804, 0.38655, 0.63859, 0.44109, 0.56706, 0.94015, 1.30876, 1.25158, 2.18376, 2.85388, 6.91827], 'r',label='75% task in Fog',linewidth=3)
	# plt.xlabel('arrivalRate/serviceRate')
	# plt.ylabel('queue length of task')
	# plt.title('task simulation for arrival Rate and queue length of task in Fog with ParetoDistribution for size of task')
	# plt.grid(True)
	# legend = plt.legend(loc='upper left', shadow=True, fontsize='x-large')
	# plt.subplot(212)
	# plt.plot(ccol, [0.0, 0.03892, 0.04324, 0.10269, 0.06881, 0.03684, 0.03842, 0.06405, 0.25194, 0.10475, 0.20287, 0.05176, 0.12259, 0.27213, 0.18074, 0.54961, 0.1612, 0.26087, 0.3689, 0.77473, 1.29412], 'k',label='100% task in Cloud',linewidth=3)
	# plt.plot(ccol, [0.0, 0.00189, 0.00683, 0.00411, 0.00285, 0.02686, 0.01224, 0.14386, 0.0118, 0.01695, 0.02344, 0.00311, 0.00137, 0.01582, 0.02927, 0.03178, 0.01546, 0.01741, 0.0464, 0.07592, 0.2966], 'b',label='25% task in Cloud',linewidth=3)
	# plt.plot(ccol, [0.0, 0.00474, 0.00399, 0.0196, 0.0066, 0.02972, 0.01459, 0.00938, 0.03244, 0.01442, 0.00681, 0.16997, 0.00962, 0.0537, 0.11914, 0.09617, 0.11137, 0.01144, 0.06029, 0.0513, 0.00744], 'g',label='50% task in Cloud',linewidth=3)
	# plt.plot(ccol, [0.0, 0.15754, 0.02125, 0.02182, 0.00901, 0.09256, 0.03469, 0.13316, 0.07864, 0.01596, 0.03769, 0.0315, 0.02685, 0.06675, 0.01838, 0.12685, 0.02829, 0.40248, 0.14256, 0.42345, 0.44799], 'r',label='75% task in Cloud',linewidth=3)
	# plt.xlabel('arrivalRate/serviceRate')
	# plt.ylabel('queue length of task')
	# plt.title('task simulation for arrival Rate and queue length of task in Cloud with ParetoDistribution for size of task')
	# plt.grid(True)
	# legend = plt.legend(loc='upper left', shadow=True, fontsize='x-large')
	# plt.show()
	
	# xcol=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
	# y100fcol=[0.0, 1357.0, 347076.0, 3356.0, 2488.0, 2552.0, 3340.0, 3403.0, 7207.0, 4222.0,12167.0, 4867.0, 5052.0, 5851.0, 6482.0, 7432.0, 7435.0, 11434.0, 7610.0, 8872.0, 8915.0]
	# y100ccol=[0.0, 544.0, 1092.0, 1415.0, 1725.0, 2103.0, 3477.0, 3292.0, 3965.0, 3834.0, 4057.0, 4880.0, 4907.0, 6200.0, 5777.0, 6038.0, 6487.0, 7136.0, 7586.0, 7793.0, 8093.0]
	# y025fcol=[0.0, 596.0, 921.0, 1431.0, 2497.0, 2262.0, 2696.0, 6365.0, 3266.0, 3613.0, 4032.0, 4457.0, 5201.0, 5481.0, 6376.0, 6112.0, 6904.0, 6993.0, 7242.0, 7622.0, 8874.0]
	# y050fcol=[0.0, 12089.0, 1189.0, 1360.0, 2536.0, 2137.0, 7081.0, 3072.0, 3259.0, 3742.0, 4742.0, 5288.0, 4856.0, 5302.0, 5759.0, 6031.0, 6528.0, 7577.0, 7244.0, 9535.0, 8014.0]
	# y075fcol=[0.0, 1089.0, 1225.0, 1638.0, 1896.0, 2523.0, 3776.0, 3109.0, 4108.0, 7302.0, 4504.0, 4907.0, 4956.0, 5594.0, 7257.0, 6210.0, 7036.0, 7014.0, 7297.0, 7613.0, 8461.0]
	# pddf=pd.DataFrame({'100% task in Cloud': y100ccol,"25% task in Fog":y025fcol,
	# "50% task in Fog":y050fcol,"75% task in Fog":y075fcol,"100% task in Fog":y100fcol},index=xcol)
	# fig=plt.figure(1)
	# ax=fig.add_subplot(111)
	# ax.set_xlabel("arrival time")
	# ax.set_ylabel("completion time")
	# pddf.plot(ax=ax,title="task simulation for arrival time and completion time with ParetoDistribution for size of task", style="-*", legend=True, grid=True, linewidth=3)
	# plt.show()
	
	# fcol=[0.0, 0.25, 0.2631578947368421, 0.27777777777777773, 0.2941176470588235, 0.3125, 0.3333333333333333, 0.3571428571428571, 0.38461538461538464, 0.41666666666666663, 0.45454545454545453, 0.5, 0.5555555555555555, 0.625, 0.7142857142857142, 0.8333333333333333, 1.0, 1.25, 1.6666666666666665, 2.5, 5.0]
	# ccol=[0.0, 0.125, 0.13157894736842105, 0.13888888888888887, 0.14705882352941174, 0.15625, 0.16666666666666666, 0.17857142857142855, 0.19230769230769232, 0.20833333333333331, 0.22727272727272727, 0.25, 0.27777777777777773, 0.3125, 0.3571428571428571, 0.41666666666666663, 0.5, 0.625, 0.8333333333333333, 1.25, 2.5]
	# plt.figure(1)
	# plt.subplot(211)
	# plt.plot(fcol, [0.0, 0.17583, 0.18508, 0.19535, 0.20684, 0.21975, 0.23439, 0.25112, 0.27042, 0.29293, 0.31953, 0.35144, 0.39043, 0.43916, 0.50179, 0.58524, 0.702, 0.87695, 15.73665, 72.53381, 129.33096], 'k',label='100% task in Fog',linewidth=3)
	# plt.plot(fcol, [0.0, 0.04422, 0.04655, 0.04913, 0.05202, 0.05527, 0.05895, 0.06316, 0.06802, 0.07368, 0.08037, 0.0884, 0.09822, 0.11049, 0.12625, 0.14727, 0.17668, 0.22076, 0.53167, 1.40574, 3.53479], 'b',label='25% task in Fog',linewidth=3)
	# plt.plot(fcol, [0.0, 0.08856, 0.09322, 0.0984, 0.10419, 0.11069, 0.11807, 0.1265, 0.13622, 0.14757, 0.16097, 0.17706, 0.19672, 0.22128, 0.25286, 0.29495, 0.35385, 0.44215, 1.52333, 4.9963, 39.02401], 'g',label='50% task in Fog',linewidth=3)
	# plt.plot(fcol, [0.0, 0.13165, 0.13858, 0.14628, 0.15488, 0.16455, 0.17552, 0.18805, 0.2025, 0.21937, 0.2393, 0.26321, 0.29243, 0.32895, 0.37589, 0.43846, 0.52603, 0.65729, 2.90307, 29.54563, 84.60456], 'r',label='75% task in Fog',linewidth=3)
	# plt.xlabel('arrivalRate/serviceRate')
	# plt.ylabel('queue length of task')
	# plt.title('task simulation for arrival Rate and queue length of task in Fog with constant normal distribution for size of task')
	# plt.grid(True)
	# legend = plt.legend(loc='upper left', shadow=True, fontsize='x-large')
	# plt.subplot(212)
	# plt.plot(ccol, [0.0, 0.07478, 0.07872, 0.08309, 0.08798, 0.09347, 0.0997, 0.10682, 0.11503, 0.12461, 0.13593, 0.14951, 0.16611, 0.18685, 0.21352, 0.24906, 0.2988, 0.37336, 0.4975, 0.74532, 33.28261], 'k',label='100% task in Cloud',linewidth=3)
	# plt.plot(ccol, [0.0, 0.01979, 0.02083, 0.02199, 0.02328, 0.02474, 0.02639, 0.02827, 0.03044, 0.03298, 0.03597, 0.03957, 0.04396, 0.04945, 0.05651, 0.06592, 0.07908, 0.09881, 0.1309, 0.15019, 0.48384], 'b',label='25% task in Cloud',linewidth=3)
	# plt.plot(ccol, [0.0, 0.03821, 0.04022, 0.04245, 0.04495, 0.04775, 0.05094, 0.05457, 0.05877, 0.06366, 0.06944, 0.07638, 0.08486, 0.09546, 0.10908, 0.12724, 0.15265, 0.19074, 0.25417, 0.37608, 2.18503], 'g',label='50% task in Cloud',linewidth=3)
	# plt.plot(ccol, [0.0, 0.05624, 0.0592, 0.06249, 0.06617, 0.0703, 0.07498, 0.08034, 0.08651, 0.09372, 0.10223, 0.11245, 0.12493, 0.14053, 0.16059, 0.18732, 0.22472, 0.2808, 0.37417, 0.56055, 15.64414], 'r',label='75% task in Cloud',linewidth=3)
	# plt.xlabel('arrivalRate/serviceRate')
	# plt.ylabel('queue length of task')
	# plt.title('task simulation for arrival Rate and queue length of task in Cloud with constant normal distribution for size of task')
	# plt.grid(True)
	# legend = plt.legend(loc='upper left', shadow=True, fontsize='x-large')
	# plt.show()
	
	# xcol=[0.053, 0.04975, 0.03325, 0.02675, 0.02325, 0.02025, 0.01875, 0.01775, 0.01675, 0.01575, 0.01475, 0.01375, 0.01325, 0.01325, 0.01325, 0.01325, 0.01325, 0.01325, 0.01325, 0.01325]
	# xxcol=[0.048, 0.04275, 0.0255, 0.019, 0.0145, 0.0125, 0.0105, 0.0085, 0.0075, 0.007, 0.0065, 0.006, 0.0055, 0.005, 0.0045, 0.004, 0.0035, 0.003, 0.0025, 0.002]
	# xxxcol=[0.29, 0.289, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25,0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25]
	# xxxxcol=[0.1575, 0.0325, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,0.0, 0.0, 0.0, 0.0, 0.0]
	# yaxis={'1': xcol, '2': xxcol, '3':xxxcol, '4':xxxxcol}
	# groups=20
	# fig,ax=plt.subplots()
	# index=np.arange(groups)
	# bar_width=0.2
	# opacity=0.4
	# plt.bar(index+bar_width*(0),yaxis[str(1)],bar_width,alpha=opacity,color='blue',label='Fog instance 1')
	# plt.bar(index+bar_width*(1),yaxis[str(2)],bar_width,alpha=opacity,color='black',label='Fog instance 2')
	# plt.bar(index+bar_width*(2),yaxis[str(3)],bar_width,alpha=opacity,color='green',label='Cloud instance 1')
	# plt.bar(index+bar_width*(3),yaxis[str(4)],bar_width,alpha=opacity,color='yellow',label='Cloud instance 2')
	# plt.xlabel('Arrival time')
	# plt.ylabel('Utilization rate')
	# plt.title('Task simulation of arrival time with Utilization rate with ContDistUniform for size of task and fog cache')
	# plt.xticks(index+bar_width,tuple([str(i+1) for i in range(groups)]))
	# plt.legend()
	# plt.tight_layout()
	# plt.show()
	
	# xcol=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
	# ycolnocahce=[0.0, 4461.0, 8458.0, 12465.0, 16466.0, 20462.0, 24463.0, 28455.0, 32457.0, 36456.0, 40459.0, 44276.0, 48013.0, 52012.0, 56011.0, 60010.0, 64009.0, 68008.0, 72007.0, 76006.0, 80005.0]
	# ycolcache=[0.0, 4436.0, 8457.0, 11997.0, 15996.0, 19995.0, 23994.0, 27993.0, 31992.0, 35991.0, 39990.0, 43989.0, 47988.0, 51987.0, 55986.0, 59985.0, 63984.0, 67983.0, 71982.0, 75981.0, 79980.0]
	# pddf=pd.DataFrame({'no cache in Fog': ycolnocahce,'cache in Fog': ycolcache},index=xcol)
	# fig=plt.figure(1)
	# ax=fig.add_subplot(111)
	# ax.set_xlabel("arrival time")
	# ax.set_ylabel("completion time")
	# pddf.plot(ax=ax,title="task simulation for arrival time and completion time with const normal distribution for size of task", style="-s", legend=True, grid=True, linewidth=2)
	# plt.show()
	
	# ycolnocahce=[0.0, 4461.0, 8458.0, 12465.0, 16466.0, 20462.0, 24463.0, 28455.0, 32457.0, 36456.0, 40459.0, 44276.0, 48013.0, 52012.0, 56011.0, 60010.0, 64009.0, 68008.0, 72007.0, 76006.0, 80005.0]
	# ycolcache=[0.0, 4436.0, 8457.0, 11997.0, 15996.0, 19995.0, 23994.0, 27993.0, 31992.0, 35991.0, 39990.0, 43989.0, 47988.0, 51987.0, 55986.0, 59985.0, 63984.0, 67983.0, 71982.0, 75981.0, 79980.0]
	# xaxis={'1': ycolnocahce, '2': ycolcache}
	# groups=21
	# fig,ax=plt.subplots()
	# index=np.arange(groups)
	# bar_width=0.2
	# opacity=0.4
	# bar1=ax.bar(index+bar_width*(0),xaxis[str(1)],bar_width,alpha=opacity,color='blue',label='no cache in Fog')
	# bar2=ax.bar(index+bar_width*(1),xaxis[str(2)],bar_width,alpha=opacity,color='black',label='cache in Fog')
	# ax.set_xlabel('Arrival time')
	# ax.set_ylabel('Completion time')
	# ax.set_title('Task simulation of arrival time with completion time with ContDistUniform for size of task')
	# ticks=tuple([str(i) for i in range(groups)])
	# ax.set_xticks(index+bar_width)
	# ax.set_xticklabels(ticks)
	# ax.legend()
	# for i in range(1,groups):
		# height1=bar1[i].get_height()
		# height2=bar2[i].get_height()
		# ax.text(bar1[i].get_x()+bar1[i].get_width()/2,height1+1000,'%d' % int(height1),ha='center',va='bottom')
		# ax.text(bar2[i].get_x()+0.3+bar2[i].get_width()/2,height2-2000,'%d' % int(height2),ha='center',va='bottom')
	# plt.show()
	
	# ycol=[0.0, 48999.0, 49025.0, 49056.0, 49079.0, 49100.0, 49129.0, 49213.0, 49386.0, 51546.0, 57234.0]
	# xaxis={'1': ycol}
	# groups=11
	# fig,ax=plt.subplots()
	# index=np.arange(groups)
	# bar_width=0.2
	# opacity=0.4
	# bar1=ax.bar(index+bar_width*(0),xaxis[str(1)],bar_width,alpha=opacity,color='blue',label='moving tasks in different percentage')
	# ax.set_xlabel('percentage')
	# ax.set_ylabel('Completion time')
	# ax.set_title('Task simulation for moving tasks')
	# ticks=tuple([str(i*10) for i in range(groups)])
	# ax.set_xticks(index+bar_width)
	# ax.set_xticklabels(ticks)
	# ax.legend()
	# for i in range(1,groups):
		# height1=bar1[i].get_height()
		# ax.text(bar1[i].get_x()+0.4+bar1[i].get_width()/2,height1-2000,'%d' % int(height1),ha='center',va='bottom')
	# plt.show()
	
	ycolfog=[0,130.15272, 446.6594, 722.95731, 946.07101, 1115.7966, 1234.22467, 1304.34243, 1321.21759, 1344.05552, 1496.43857]
	ycolfogtransfer=[0,0.8749, 32.06068, 97.60343, 195.57338, 325.9267, 488.78654, 683.48759, 907.8619, 1118.03067, 1258.37789]
	xaxis={'1': ycolfog, '2': ycolfogtransfer}
	groups=11
	fig,ax=plt.subplots()
	index=np.arange(groups)
	bar_width=0.2
	opacity=0.4
	bar1=ax.bar(index+bar_width*(0),xaxis[str(1)],bar_width,alpha=opacity,color='blue',label='Fog instance 2')
	bar2=ax.bar(index+bar_width*(1),xaxis[str(2)],bar_width,alpha=opacity,color='black',label='Fog 1 transfer queue')
	ax.set_xlabel('percentage of moving tasks')
	ax.set_ylabel('queue on the fog instance and transmission queue between fog')
	ax.set_title('Task simulation for moving tasks')
	ticks=tuple([str(i*10) for i in range(groups)])
	ax.set_xticks(index+bar_width)
	ax.set_xticklabels(ticks)
	ax.legend()
	for i in range(1,groups):
		height1=bar1[i].get_height()
		height2=bar2[i].get_height()
		ax.text(bar1[i].get_x()+0.3+bar1[i].get_width()/2,height1-100,'%d' % int(height1),ha='center',va='bottom')
		ax.text(bar2[i].get_x()+0.3+bar2[i].get_width()/2,height2,'%d' % int(height2),ha='center',va='bottom')
	plt.tight_layout()
	plt.show()