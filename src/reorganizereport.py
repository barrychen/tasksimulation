import os,shutil

if __name__ == '__main__':
	dirPath="D:/workspace/TaskSimulation"
	tempPath="%s/%s" % (dirPath,"temp")
	reportDir="%s/%s" % (dirPath,"report/parallel")
	if not os.path.isdir(reportDir):
		os.mkdir(reportDir)
	tfileList=os.listdir(tempPath)
	fileList=[sfile for sfile in tfileList if "IOTExperiment" in sfile]
	categoryNameList=set([cname.split("_")[1] for cname in tfileList if "IOTExperiment" in cname])
	for cname in categoryNameList:
		cdir="%s/%s" %(reportDir,cname)
		if not os.path.isdir(cdir):
			os.mkdir(cdir)
	for oneFile in fileList:
		categoryName=str(oneFile.split("_")[1])
		shutil.copyfile(("%s/%s" % (tempPath,oneFile)),("%s/%s/%s" % (reportDir,categoryName,oneFile)))